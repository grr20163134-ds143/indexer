# indexer

Main activity for DS143 evaluation

## Use
- place txt files alongside indexer.exe 

## Options
- indexer --freq N ARQUIVO
- indexer --freq-word PALAVRA ARQUIVO
- indexer --search TERMO ARQUIVO_A ARQUIVO-B ... ARQUIVO_X

### Run on Linux:
- run 'chmod +x compile.sh'
- run executable with correct options 

### Run on Windows:
- run 'gcc -o indexer main.c file_operations.c hashmap.c word.c'
- run executable with correct options